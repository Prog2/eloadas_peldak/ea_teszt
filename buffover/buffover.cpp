/**
 *  \file: buffover.cpp
 *   (Latin-2 kodolasu fajl. Allitsa at a kodolast,
 *    ha a tovabbi kommentek nem olvashatok helyesen!)
 *
 *  \brief Buffer overflow hiba bemutat�sa
 *
 *  A helyes jelsz�: titok
 *  Pr�b�lja ki helyes �s helytelen jelsz�val,
 *  majd 7-10 bet� hossz�s�g� tetsz�leges jelsz�val is!
 *
 */

#include <iostream>
#include <cstring>
#include "secret.h"

using std::cin;
using std::cout;
using std::endl;

char buff[6];
bool pass = false;

int main(void) {
    initOutputEncoding();    // be�ll�tjuk a k�dk�szletet a kimeneten
    cout << "Ha tudja a jelsz�t, megn�zheti a ZH k�rd�st!\n";
    cout << "Jelszo: ";
    cin >> buff;

    if (strcmp(buff, "titok") != 0) {
        cout << "*** Hibas jelszo ***" << endl;
    } else {
        cout << "Helyes jelszo" << endl;
        pass = true;
    }

    if (pass) {
        cout << ">> Hozzaferhet a titokhoz: <<" << endl;
        printSecret();  // ki�rja a titkos inform�ci�t
    }
    return 0;
}
