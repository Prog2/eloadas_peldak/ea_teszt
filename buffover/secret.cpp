/**
 *  \file: secret.cpp
 *   (Latin-2 kodolasu fajl. Allitsa at a kodolast,
 *    ha a tovabbi kommentek nem olvashatok helyesen!)
 *
 *  \brief P�da az �kezetes karakterek megjelen�t�s�re
 *
 *   UTF-8 kezel�se bonyolultabb, mert 1 karakter t�bb byte hossz� is lehet.
 *   LINUX-ban a LANG paranccsal be kell �ll�tani a f�jl k�dol�s�nak
 *   megfelel� nyelvet (pl: LANG=hu_HU, vagy LANG=hu_HU.utf8
 *
 *   Ha t�voli termi�lemul�ci�val csatlakozunk (pl. ssh, putty), akkor az
 *   �kezethelyes megjelen�shez annak a k�dk�szlet�t is j�l kell be�ll�tani.
 *   (pl: putty -> change settings -> translation -> latin2)
 *
 *   K�dkonverzi�: iconv (van Windows �s Linux v�ltozata is)
 */

#include <iostream>
#include <clocale>
#define WINX defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#if WINX
#include <windows.h>
#endif // WINX
#include "secret.h"

using std::cout;
using std::endl;

void printSecret() {
    cout << "1. Mikor jelentkezik a buffer oveflow hiba?\n"
         << "\ta) reggel\n"
         << "\tb) aritmetikai t�lcsordul�skor\n"
         << "\tc) ha elfogy a mem�ria\n"
         << "\td) egy v�ltoz�t (buffert) a program hosszabban �r, mint annak a m�rete" << std::endl;
}

void initOutputEncoding() {
    setlocale(LC_ALL, "");
#if WINX
/// Windows alatt egy�b var�zslatra is sz�ks�g van ...
    SetConsoleCP(1250);
    SetConsoleOutputCP(1250);
#endif
}
